/* -copyright-
#-# Copyright © 2021 Eric Bina, Dave Black, TJ Phan, 
#-#    Vincent Renardias, Jim Rees, Willem Vermin
#-# 
#-# Permission is hereby granted, free of charge, to any person 
#-# obtaining a copy of this software and associated documentation 
#-# files (the “Software”), to deal in the Software without 
#-# restriction, including without limitation the rights to use, 
#-# copy, modify, merge, publish, distribute, sublicense, and/or 
#-# sell copies of the Software, and to permit persons to whom 
#-# the Software is furnished to do so, subject to the following 
#-# conditions:
#-# 
#-# The above copyright notice and this permission notice shall 
#-# be included in all copies or substantial portions of the Software.
#-# 
#-# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, 
#-# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
#-# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#-# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
#-# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#-# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
#-# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
#-# OTHER DEALINGS IN THE SOFTWARE.
#-# 
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <stdio.h>
#include <math.h>
#include "utils.h"
#include "debug.h"
#include "xfishtank.h"
#include "xdo.h"

ssize_t mywrite(int fd, const void *buf, size_t count)
{
   const size_t m = 4096; // max per write
   size_t w       = 0;    // # written chars           
   char *b        = (char *)buf;

   while (w < count)
   {
      size_t l = count - w;
      if (l > m)
	 l = m;
      ssize_t x = write(fd, b+w, l);
      if (x < 0)
	 return -1;
      w += x;
   }
   return 0;
}


int is_little_endian(void)
{
   int endiantest = 1;
   return (*(char *)&endiantest) == 1;
}

// find largest window with name
Window  largest_window_with_name(xdo_t *myxdo, const char *name)
{
   xdo_search_t search;

   memset(&search,0,sizeof(xdo_search_t));

   search.searchmask = SEARCH_NAME;
   search.winname    = name;
   search.limit      = 0;
   search.require    = SEARCH_ANY;
   search.max_depth  = 4;

   Window *windows = NULL;
   unsigned int nwindows;

   xdo_search_windows(myxdo, &search, &windows, &nwindows);
   P("nwindows: %s %d\n",search.winname,nwindows);
   unsigned int i;
   Window w = 0;
   unsigned int maxsize = 0;

   for (i=0; i<nwindows; i++)
   {
      P("window: 0x%lx\n",windows[i]);
      unsigned int width,height;
      xdo_get_window_size(myxdo, windows[i], &width, &height);
      unsigned int size = width*height;
      if (size <= maxsize)
	 continue;
      P("width %d height %d size %d prev maxsize %d\n",width,height, size, maxsize);
      maxsize = size;
      w = windows[i];
   }
   if (windows)
      free(windows);
   return w;
}

void rgba2color(GdkRGBA *c, char **s)
{
   *s = (char *)malloc(8);
   sprintf(*s,"#%02lx%02lx%02lx",lrint(c->red*255),lrint(c->green*255),lrint(c->blue*255));
}

