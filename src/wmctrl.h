/* -copyright-
#-# Copyright © 2021 Eric Bina, Dave Black, TJ Phan, 
#-#    Vincent Renardias, Jim Rees, Willem Vermin
#-# 
#-# Permission is hereby granted, free of charge, to any person 
#-# obtaining a copy of this software and associated documentation 
#-# files (the “Software”), to deal in the Software without 
#-# restriction, including without limitation the rights to use, 
#-# copy, modify, merge, publish, distribute, sublicense, and/or 
#-# sell copies of the Software, and to permit persons to whom 
#-# the Software is furnished to do so, subject to the following 
#-# conditions:
#-# 
#-# The above copyright notice and this permission notice shall 
#-# be included in all copies or substantial portions of the Software.
#-# 
#-# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, 
#-# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
#-# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#-# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
#-# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#-# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
#-# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
#-# OTHER DEALINGS IN THE SOFTWARE.
#-# 
*/
#pragma once
#include <X11/Xlib.h>
typedef struct _WinInfo
{
   Window id              ;
   int x,y                ; // x,y coordinates
   int xa,ya              ; // x,y coordinates absolute
   unsigned int w,h       ; // width, height
            int ws        ; // workspace

#ifdef NO_USE_BITS
   unsigned int sticky    ; // is visible on all workspaces
   unsigned int dock      ; // is a "dock" (panel)
   unsigned int hidden    ; // is hidden (iconified)
#else
   unsigned int sticky:  1; // is visible on all workspaces
   unsigned int dock  :  1; // is a "dock" (panel)
   unsigned int hidden:  1; // is hidden (iconified)
#endif
} WinInfo;

extern int          GetWindows(WinInfo **w, int *nw);

