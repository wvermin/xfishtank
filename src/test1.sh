#!/bin/sh
# -copyright-
#-# Copyright © 2021 Eric Bina, Dave Black, TJ Phan, 
#-#    Vincent Renardias, Jim Rees, Willem Vermin
#-# 
#-# Permission is hereby granted, free of charge, to any person 
#-# obtaining a copy of this software and associated documentation 
#-# files (the “Software”), to deal in the Software without 
#-# restriction, including without limitation the rights to use, 
#-# copy, modify, merge, publish, distribute, sublicense, and/or 
#-# sell copies of the Software, and to permit persons to whom 
#-# the Software is furnished to do so, subject to the following 
#-# conditions:
#-# 
#-# The above copyright notice and this permission notice shall 
#-# be included in all copies or substantial portions of the Software.
#-# 
#-# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, 
#-# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES 
#-# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
#-# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
#-# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
#-# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
#-# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR 
#-# OTHER DEALINGS IN THE SOFTWARE.
#-# 

XFISHTANK=xfishtank
if [ -x ./xfishtank ]; then
   XFISHTANK=./xfishtank
fi
# test if 'xfishtank -h' more or less works:
$XFISHTANK -h | grep -q -i xfishtank 
if [ $? -ne 0 ] ; then
   echo "Error in executing: $XFISHTANK -h"
   exit 1
fi
